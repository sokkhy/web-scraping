from flask import Flask
from flask.globals import request
from pandas.io import parsers
from flask_restful import Resource, Api, reqparse
import pandas as pd
import ast 

app = Flask(__name__)
api = Api(app)

class Users(Resource):
    #pass
    def get(self):
        data = pd.read_csv("users.csv")
        data = data.to_dict()
        return {"data" :data},200

    def post(self):
        parser = reqparse.RequestParser()  # initialize
        

        # add args
        parser.add_argument('userId', required=True) 
        parser.add_argument('name',   required=True)
        parser.add_argument('city',   required=True)

        args = parser.parse_args()

        #read csv
        data = pd.read_csv("users.csv")
        
        if(args["userId"] in list(data["userId"])):
            return{
                "message " : f"'{args['userId']}' already exist"
            }, 401
        else:

            # create new dataframe containing new values
            new_data = pd.DataFrame({
                "userId" : args["userId"],
                "name" : args["name"],
                "city" : args["city"],
                "location" : [[]]
            })
            # add the newly provided values
            data = data.append(new_data, ignore_index=True)
            # save back to CSV
            data.to_csv("users.csv", index=False)
            return {"data" : data.to_dict()},200



# class Locations(Resource):
#     pass

# api.add_resource(Users, '/users') # '/users' is our entry point
# api.add_resource(Locations, '/locations') # '/users' is our entry point

# if __name__ == "__main__":
#     app.run()