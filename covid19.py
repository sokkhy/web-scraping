from __future__ import print_function
from tabulate import tabulate
import argparse
import csv
import pprint
import matplotlib.pyplot as plt 
# from MechanicalSoup import mechanicalsoup
import mechanicalsoup
from prettytable import PrettyTable
# hide user input
import re
from getpass import getpass

from flask import Flask
from flask.globals import request
from pandas.io import parsers
from flask_restful import Resource, Api, reqparse
import pandas as pd
import ast 




getAllCountry    = {}
CountryNameAll   = []
infectCountryCnt = []


x = [1,2,3,4,5,6] 
# corresponding y axis values 
y = [2,4,1,5,2,6] 
  
# plotting the points  
plt.plot(x, y, color='green', linestyle='dashed', linewidth = 3, 
         marker='o', markerfacecolor='blue', markersize=12) 
  
# setting x and y axis range 
plt.ylim(1,8) 
plt.xlim(1,8) 
  
# naming the x axis 
plt.xlabel('x - axis') 
# naming the y axis 
plt.ylabel('y - axis') 
  
# giving a title to my graph 
plt.title('Some cool customizations!') 
# function to show the plot 
# plt.show() 

def openSite(url):
    
    f = open('covid19_infected_country.csv', 'w',encoding='utf-8')
    browser = mechanicalsoup.StatefulBrowser(
        soup_config={'features': 'lxml'},
        raise_on_404=True,
        user_agent='MyBot/0.1: mysite.example.com/bot_info',
    )
    # Uncomment for a more verbose output:
    # browserset_verbose(2)
    browser.open(url)
    page                         = browser.get_current_page()
    # containSearchCountryTodayDiv = page.find("div", class_="tab-pane active")
    # containSearchCountryToday   = page.find("div", class_="main_table_countries_div")
    
    findClass = page.find("div", class_="active")
    if findClass:         
        containSearchCountryTable       = page.find("table", id="main_table_countries_today")
       # print(".............",containSearchCountryTable )
        if containSearchCountryTable:
            containSearchCountryTr      = containSearchCountryTable.findAll("tr")
            #test    =  containSearchCountryTr.find("tr", class_="odd even")
            #print("test " , test)
            length                      = len(containSearchCountryTr)
            print("size : ",length)
          #  try:
            for getData in range(1,length,1):
                # for containSearchCountryTr in containSearchCountryTable:
                    # print("true")
                #for getData in range(1,length,1):
                    result          = []
                #   print("result" , getData)
                    CountryIndex     = containSearchCountryTr[getData].findAll("td")[0].text.strip()
                    CountryName     = containSearchCountryTr[getData].findAll("td")[1].text.strip()
                    CountryCase     = containSearchCountryTr[getData].findAll("td")[2].text.strip()
                    CountryNewCase  = containSearchCountryTr[getData].findAll("td")[3].text.strip()
                    CountryDeath    = containSearchCountryTr[getData].findAll("td")[4].text.strip()
                    CountryRecoCase = containSearchCountryTr[getData].findAll("td")[6].text.strip()
                    CountryActCase  = containSearchCountryTr[getData].findAll("td")[7].text.strip()   
                    if CountryDeath == "" or CountryDeath == "N/A":
                        CountryDeath = "0"
                    TotalDeathRate =  int(CountryDeath.replace(",",""))* 100 /  int(CountryCase.replace(",",""))
                    if CountryRecoCase == "" or CountryRecoCase == "N/A":
                        CountryRecoCase = "0"
                    RecoverdRate   = int(CountryRecoCase.replace(",",""))* 100 /  int(CountryCase.replace(",",""))
                   
                    result.append(CountryIndex)
                    result.append(CountryName.replace(" ","").capitalize())
                    result.append(CountryCase)
                    result.append(CountryNewCase)
                    result.append(CountryDeath)
                    result.append(CountryRecoCase)
                    result.append(str(round(RecoverdRate,3))) #3 digit decimal 
                    result.append(CountryActCase)
                    result.append(str(round(TotalDeathRate,3))) #3 digit decimal 

                    CountryNameAll.append(CountryName.capitalize())
                    infectCountryCnt.append(CountryName.capitalize())
                    getAllCountry[CountryName.replace(" ","").capitalize()] = result
                    #print("CountryName " ,getAllCountry)
                    
            with f:
                writer = csv.writer(f)
                for row in getAllCountry:
                    writer.writerow(getAllCountry[row])
           # except ValueError:
            #    print("Error")

def searchCountry():
    printTable              = PrettyTable()
    printTable.field_names  = ['Index','Country', 'Total Case','New Case', 'Death','Recovered','Recover Rate(%)','Active Case','Death Rate (%)']
    _country = []
    _country.append(input("Search Infected Country : "))
   # print("country",_country)
    try:
        for inCountry in _country:
            #print(getAllCountry)
            if(inCountry == "exit"):
                option() 
                print("inCountry " ,inCountry)
            else:
                getAllCountry.get(inCountry)
                printTable.add_row(getAllCountry.get(inCountry.capitalize()))
                print(printTable)
                searchCountry()
    except :
        
        for inCountry in _country:
            print(inCountry.capitalize(), " not a country")
        searchCountry()

def option():
    try:
        openSite("https://www.worldometers.info/coronavirus/#countries")
        print("1.All Counry \n2.Search Country")
        getOption = input("Choose your opiotn : ")
        #openSite("https://www.worldometers.info/coronavirus/#countries")
        printTable              = PrettyTable()
        printTable.field_names  = ['index','Country', 'Total Case','New Case', 'Death','Recovered','Recover Rate(%)','Active Case','Death Rate']
        if getOption == '1':
            print("Number of Infected Country: ", len(infectCountryCnt) -1)
            # print("Case Outside China: ", int(getAllCountry.get("Total:")[1].replace(",","")) - int(getAllCountry.get("China")[1].replace(",","")))
            print("CountryNameAll " , len(CountryNameAll))
            for getCountry in getAllCountry:
                print(getCountry)
                #printTable.add_row(getAllCountry.get(getCountry))
        elif  getOption == '2':
            searchCountry()
        else:
            print( getOption , "not a feature. Please input 1 or 2.")
            option()
    except:
        print("error")    

# show result 
option()  

# app = Flask(__name__)
# api = Api(app)    

# class Users(Resource):
#     #pass
#     def get(self):
#         data = pd.read_csv("covid19_infected_country.csv")
#         data = data.to_dict()
#         return {"data" :data},200 

# api.add_resource(Users, '/users') # '/users' is our entry point
# #api.add_resource(Locations, '/locations') # '/users' is our entry point

# if __name__ == "__main__":
#     app.run()